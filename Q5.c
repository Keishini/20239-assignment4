#include <stdio.h>

int main()
 {
 	int A=10,B=15,i=3;
 	printf("Output = %d\n",A&B);
 	printf("Output = %d\n",A^B);
 	printf("Complement = %d\n",~A);
 	printf("Left shift by %d : %d\n",i,A<<i);
 	printf("Right shift by %d : %d\n",i,B>>i);
	return 0;
}
