#include <stdio.h>

int main()
 {
 	
 	const float PI = 3.14;
 	float r,h,volume;
 	printf("Enter radius of the cone:");
 	scanf("%f",&r);
 	printf("Enter height of the cone:");
 	scanf("%f",&h);
 	volume=(PI*r*r*h)/3.0;
 	printf("Volume of the cone is %f\n",volume);
	return 0;
}
